$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
      interval: 1000
    });
    $('#contacto').on('show.bs.modal', function (e) {
      // do something...
      console.log('El modal se esta mostrando');
      $('#contactoBtn').removeClass('btn-outline-success');
      $('#contactoBtn').addClass('btn-primary');
      $('#contactoBtn').prop('disabled',true);

    });
    $('#contacto').on('shown.bs.modal', function (e) {
      // do something...
      console.log('El modal se mostró');
    });
    $('#contacto').on('hide.bs.modal', function (e) {
      // do something...
      console.log('El modal se esta ocultado');
    });
    $('#contacto').on('hidden.bs.modal', function (e) {
      // do something...
      console.log('El modal se ocultó');
      $('#contactoBtn').prop('disabled',false);
    });
});